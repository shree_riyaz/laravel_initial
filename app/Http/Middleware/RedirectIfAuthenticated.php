<?php

namespace App\Http\Middleware;

use Closure;
use App\Model\User;
use App\Model\RoleUser;
use App\ACME\Admin\AdminHelper;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated extends AdminHelper
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {

            $roleId = $this->authenticateUser();
            if($roleId == 1) {

                return redirect('admin/');
            }
            else{

                return redirect('login');
            }
        }

        return $next($request);
    }


}
